export declare const time: () => number;
export declare const getStampInMilliseconds: () => number;
export declare const getOneYearInSeconds: () => number;
export declare const secondsToMilliseconds: (n: number) => number;
export declare const dateToTimestamp: (date: Date) => number;
